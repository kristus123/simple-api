const { Pool } = require('pg');
const pool = new Pool({

})


class Database {
    constructor() {
        this.config = {
            connectionString: (process.env.DATABASE_URL ? process.env.DATABASE_URL : "postgres://postgres:root@localhost:5432/planets"),
            ssl: (process.env.DATABASE_URL ? true : false)
        };
        this.pool = new Pool(this.config);
    }

    getPool() {
        return this.pool;
    }

    getAllPlanets(req, resp) {
        this.pool.query('SELECT * FROM planet', (error, results) => {
            if (error) {
                console.log(error);
            }
            resp.json(results.rows);
        })
    }

    insertPlanet(req, resp) {
        this.pool.query('INSERT INTO planet (date_discovered, name, size) VALUES ($1, $2, $3)', [req.body.date_discovered, req.body.name, req.body.size]);
        resp.json({ success: true })
    }


    getOnePlanet(req, resp) {
        this.pool.query('SELECT * FROM planet WHERE id = $1 ', [req.body.planetID], (error, results) => {
            if (error) {
                console.log(error);
            }
            if (results.rows[0] != null) {
                resp.json(results.rows);
            } else {
                resp.json({ success: 'not finding planet by id' })
            }
            resp.end();

        })
    }


    deletePlanet(req, resp) {

        this.pool.query('SELECT FROM planet WHERE id = $1 ', [req.body.planetID], (error, results) => {
            if (results.rows[0] == null) {
                resp.json({ msg: 'Alaready deleted' });
            } else {
                this.pool.query('DELETE FROM planet WHERE id = $1 ', [req.body.planetID], (error, results) => {
                    if (results.rows[0] == null) {
                        resp.json({ success: 'success' });
                    } else {
                        resp.json({ success: 'not finding planet by id' })
                    }
                    resp.end();

                })
            }
        })








    }

    //DELETE FROM planet WHERE id = $1', [req.body.planetID]);

    updatePlanet(req, resp) {
        this.pool.query('UPDATE planet SET date_discovered = $2, name = $3, size = $4 WHERE id = $1', [req.body.planetID, req.body.date_discovered, req.body.name, req.body.size]);
        resp.json({ success: true })
    }


    //UPDATE table_name SET column1 = value1, column2 = value2...., columnN = valueN WHERE [condition];



}



module.exports.Database = new Database();