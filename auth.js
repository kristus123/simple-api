const crypto = require('crypto');

class Auth {
    constructor(Pool) {
        this.pool = Pool;
    }



    async authenticate(request, response) {
        try {
            const { authorization } = request.headers;

            if (!authorization) {
                return "no API key found"
            }


            const key = authorization.split(" ")[1];
            console.log(key)
            const keyResult = await this.pool.query("SELECT * FROM api_key where key = $1 AND active = 1", [key]);
            console.log("___")
            console.log(keyResult);
            console.log("___")

            if (keyResult.rowCount > 0) {
                return true
            } else {
                return {
                    error: "invalid API"
                }
            }




        } catch (e) {
            return {
                error: e.message
            }

        }
    }




    async generateKey(req, resp) {
        try {
            const apiKey = await crypto.randomBytes(32).toString('hex');
            const result = await this.pool.query("INSERT INTO api_key (key) VALUES($1) RETURNING ID ", [apiKey])
            resp.json({
                key: apiKey
            })
        } catch (e) {
            resp.json({
                peepee: "POPOO",
                error: e.message
            })
    
        }
    
    
    }
    
    async authenticateKey() {
    
    }



}



module.exports.Auth = Auth;