const express = require('express');
const { Database } = require("./db");
const { Auth } = require("./auth")

const app = express()
const port = process.env.PORT || 8080;





app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));


const auth = new Auth(Database.getPool());


const publicPath = [
    "/api/v1/auth/generate-key"
    // "/api/v1/planet/getAllPlanets"
];


app.use(async (req, resp, next) => {
    console.log(req.originalUrl);
    if (publicPath.includes(req.originalUrl)) {
        return next();
    }

    const authResult = await auth.authenticate(req, resp);
    if (authResult === true) {
        next();
    } else {
        resp.status(401).json(authResult);
    }
});





app.get("/", (req, res) => {
    res.send(`velkommen til vår API ${process.env.DATABASE_URL}`)
})

app.get("/api/v1/planet/getAllPlanets", (req, resp) => {
    Database.getAllPlanets(req, resp);

})



app.get("/api/v1/planet/getOnePlanet", (req, resp) => {
    Database.getOnePlanet(req, resp);

})



app.post("/api/v1/planet/insertPlanet", (req, resp) => {
    console.log(req.body);
    Database.insertPlanet(req, resp);

})

app.delete("/api/v1/planet/deletePlanet", (req, resp) => {
    console.log(req.body);
    Database.deletePlanet(req, resp);
})

app.patch("/api/v1/planet/updatePlanet", (req, resp) => {
    console.log(req.body);
    Database.updatePlanet(req, resp);
})


//AUTHENTICATION

app.post("/api/v1/auth/generate-key", (request, response) => {
    //response.send("ehiehi");
    // console.log("AHAHAasdasd");
    auth.generateKey(request, response)
});


app.listen(port, () => console.log(`Example app listening on port ${port}!`))